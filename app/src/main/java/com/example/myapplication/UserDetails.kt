package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class UserDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)


        var userimage = findViewById<ImageView>(R.id.user_iv)
        var userNameTv = findViewById<TextView>(R.id.name_tv)
        var emailTv = findViewById<TextView>(R.id.email_tv)


        var firstNmae =intent.getStringExtra("firstname")
        var lastName =intent.getStringExtra("lastname")
        var email =intent.getStringExtra("email")
        var image =intent.getStringExtra("image")


        Glide.with(this)
            .load(image)
//            .circleCrop()
            .into(userimage)

        userNameTv.text= "$firstNmae $lastName"
        emailTv.text=email


    }
}