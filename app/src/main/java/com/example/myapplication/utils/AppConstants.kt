package com.example.myapplication.utils

object AppConstants {
    val LOGIN: String?="LOGIN"
    val LATTITUTDE_LONGITIDE: String?="LATTITUTDE_LONGITIDE"

    const val REQUEST_CODE_STORAGE_PERMS = 321
    const val DB_NAME = "safeApp.db"
    const val DEVICE_TYPE = "1"


    val NULL_INDEX = -1L
    val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"

    val PREF_NAME = "safeApp_pref"






}// This utility class is not publicly instantiable
