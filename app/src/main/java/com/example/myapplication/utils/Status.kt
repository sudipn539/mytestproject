package com.example.myapplication.utils;

enum class Status {
    SUCCESS,
    FAIL,
    ERROR,
    LOADING
}