package com.example.myapplication.utils;

public interface OnItemClickReturnBooleanPermissionRationale {
    void onItemClick(Boolean status);
}
