package com.example.myapplication.utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.example.myapplication.R;

//import com.facefirst.R;


public class AlertDialogHelperForPermission {


    public static void showDialogWithYesNoCallback(Context mContext, String title, String description, final OnItemClickReturnBooleanPermissionRationale onItemClickReturnBooleanPermissionRationale){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(description)
                .setTitle(title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onItemClickReturnBooleanPermissionRationale.onItemClick(true);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onItemClickReturnBooleanPermissionRationale.onItemClick(false);
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

}
