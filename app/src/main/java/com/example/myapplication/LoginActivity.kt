package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.TextView
import android.widget.Toast
import com.example.myapplication.api.UserListResponse
import com.example.myapplication.utils.AppConstants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    var apiInterface: APIInterface? = null
    var email :String? =null
   var  password :String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        apiInterface = ApiClient.getClient()!!.create(APIInterface::class.java)

        var submitBt=findViewById<TextView>(R.id.loginBt)
        var emailEt=findViewById<TextView>(R.id.editTextTextPersonName)
        var passwordEt=findViewById<TextView>(R.id.passwordEt)


        submitBt.setOnClickListener {
             email=emailEt.text.toString()
                 password=passwordEt.text.toString()


            if(email.isNullOrEmpty()){
                Toast.makeText(this,"please enter email ", Toast.LENGTH_LONG).show()
            }else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                Toast.makeText(this,"please enter valid email ", Toast.LENGTH_LONG).show()


            else if(password.isNullOrEmpty()){
                Toast.makeText(this,"password can't be blank", Toast.LENGTH_LONG).show()
            }else{


//                Toast.makeText(this,"ok",Toast.LENGTH_LONG).show()

                var user=User()
                user.email=email
                user.password=password
//               callApiUserListing(user)
//                loginViewModel.login(commonRequest)

                callApiUserListing(user!!)

            }
        }


//        callApiUserListing()
    }


    private fun callApiUserListing(user: User) {

//        var user=User()
//        user.email="eve.holt@reqres.in"
//        user.password="cityslicka"
        val call3: Call<User?>? = apiInterface!!.login(user)
        call3!!.enqueue(object : Callback<User?> {
            override fun onResponse(
                call: Call<User?>?,
                response: Response<User?>
            ) {

                if (response.code() == 200) {
                    var user = response.body()

                    Log.d("response", "--" + user!!.token!!)
                    Toast.makeText(this@LoginActivity,"successfully logged in",Toast.LENGTH_LONG).show()

                    val editor: SharedPreferences.Editor =
                        getSharedPreferences(AppConstants.LOGIN, Context.MODE_PRIVATE).edit()
                    editor.putString("token", user!!.token!!)
                    editor.putString("email",email)
                    editor.putString("password",password)

//                editor.putString("longitude", longitude.toString())
                    editor.apply()

                    startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                    finish()
                } else {

                    Toast.makeText(this@LoginActivity!!, "user not found", Toast.LENGTH_LONG).show()

                }
            }

            override fun onFailure(call: Call<User?>, t: Throwable?) {
                call.cancel()
            }
        })    }

}