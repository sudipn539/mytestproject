package com.example.myapplication

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import com.example.myapplication.utils.AppConstants
import com.facefirst.utils.PermissionResultCallback
import com.facefirst.utils.PermissionUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class SplashActivity : AppCompatActivity() , PermissionResultCallback {
    private val CHECK_LOCATION_SETTINGS_REQUEST_CODE: Int = 1

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var permissionUtils: PermissionUtils? = null
    private var permissions = ArrayList<String>()
    var mLocationRequest: LocationRequest? = null
    private var mLocationCallback: LocationCallback? = null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_splash)
//    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
                setContentView(R.layout.activity_splash)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

//        activityAddProductBinding = viewDataBinding!!
//        splashViewModel.navigator = this

        Log.d("oncreate","-==")
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//        permissionUtils = PermissionUtils(this@SplashActivity)
        permissionUtils = PermissionUtils(this@SplashActivity,this)

//        permissions= ArrayList();
//        MainActivityPermissionsDispatcher.enableLocationWithCheck(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w : Window = getWindow();
//val w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
//        if (getIntent().getExtras() != null ) {
//            for (String key : getIntent().getExtras().keySet()) {
//
//                Log.d("field", "--" + key);
//                if (key.equals("message")) {
//                     value = getIntent().getExtras().getString(key);
//                     break;
//
//                }
//            }
//            Intent i;
//            Log.d("TAG", "Key: "  + " Value:-- " + value);
//            if (value != null) {
//
//
//                startActivity(new Intent(SplashActivity.this,HomeActivity.class).putExtra("notification",value));
//                // firebaseMessagingService.sendNotification(value,remoteMessage);
//            }
//        }


//        if (getIntent()!=null && getIntent().getExtras()!=null &&
//                getIntent().getExtras().getString("notification")!=null){
//            Log.d("check_noti_1",": "+getIntent().getExtras().getString("notification"));
////            SharedPrefManager.getInstance(HelpSupportActivity.this).saveBooleanForNotificationRedirection(true);
//        }else {
////            SharedPrefManager.getInstance(HelpSupportActivity.this).saveBooleanForNotificationRedirection(false);
//        }


        //
        /*PackageManager pm = getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            permissions.add(Manifest.permission.CAMERA);
        }
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);*/
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)


        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    // Update UI with location data
                    // ...
                    Log.d("latitude2", ":" + location.latitude)
                    Log.d("longitude2", ":" + location.longitude)
                    try {
                        val locationJson = JSONObject()
                        locationJson.put("latitude", location.latitude)
                        locationJson.put("longitude", location.longitude)
                        Log.d("latitude3", ":" + location.latitude)
                        Log.d("longitude3", ":" + location.longitude)
                        saveLatLongToLocal(location.latitude, location.longitude)
                        stopLocationUpdates()
                        //moving to login activity
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        }

        CheckPermissionAndGo()

//        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
//        permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//        permissions.add(android.Manifest.permission.CAMERA)




//        if (checkDrawOverlayPermission()) {
//        Log.d(" shredPref" , "" + splashViewModel.dataManager.currentUserId)
//
//            Handler().postDelayed({
//              //  if(splashViewModel.dataManager.currentUserLoggedInMode!=3) {
//                    startActivity(Intent(this@SplashActivity , SplashSecondScreenActivity::class.java))
//                    finish()
//
//            }, 2000)
//    }


    }
    private fun stopLocationUpdates() {
        mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
    }

//    fun checkDrawOverlayPermission(): Boolean {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            return true
//        }
//        if (!Settings.canDrawOverlays(this)) {
//            val intent = Intent(
//                Settings.ACTION_MANAGE_OVERLAY_PERMISSION ,
//                Uri.parse("package:$packageName"))
//            startActivityForResult(intent , REQUEST_CODE)
//            return false
//        } else {
//            return true
//        }
//    }

    override fun PermissionGranted(request_code: Int) {
//        showLoading()
        GoToNext()
//        TODO("Not yet implemented")
    }

    override fun PartialPermissionGranted(request_code: Int, granted_permissions: ArrayList<String>) {
        Log.d("requestcode", "==PartialPermissionGranted$request_code")

        permissions = ArrayList()
        for (i in granted_permissions.indices) {
            when (granted_permissions[i]) {
                Manifest.permission.ACCESS_FINE_LOCATION -> permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }
        }
        permissionUtils!!.check_permission(permissions, "Need These permissions", AppConstants.REQUEST_CODE_STORAGE_PERMS)

    }

    override fun PermissionDenied(request_code: Int) {
        Log.d("requestcode", "==PermissionDenied$request_code")
    }


    override fun NeverAskAgain(request_code: Int) {
        Log.d("requestcode", "==NeverAskAgain$request_code")

        GoToNext()

    }

    override fun onResume() {


        super.onResume()
//        permissionUtils = PermissionUtils(this@SplashActivity,this)
//
//        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)



    }
    private fun CheckPermissionAndGo() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            GoToNext()
        } else {
            getPermissionTo()
        }
    }
    private fun GoToNext() {
        gettingLocationAfterPermissionGranted()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun getPermissionTo() {
        permissionUtils!!.check_permission(permissions, "Need These permissions", AppConstants.REQUEST_CODE_STORAGE_PERMS)
    }

    private fun goToNextActivity() {

        Handler().postDelayed({
            runOnUiThread {


                val prefs: SharedPreferences =getSharedPreferences(AppConstants.LOGIN, Context.MODE_PRIVATE)
              var   token =
                        prefs.getString("token", "") //"No name defined" is the default value.

               if(token.isNullOrEmpty()) {
                   startActivity(Intent(this, LoginActivity::class.java))
                   finish()
               }else{
                   startActivity(Intent(this, MainActivity::class.java))
                   finish()
               }
            }
        }, 200)

    }



    private fun saveLatLongToLocal(latitude: Double, longitude: Double) {


        val editor: SharedPreferences.Editor =
            getSharedPreferences(AppConstants.LATTITUTDE_LONGITIDE, Context.MODE_PRIVATE).edit()
        editor.putString("latitude", latitude.toString())
        editor.putString("longitude", longitude.toString())
        editor.apply()
       // splashViewModel.dataManager.lattitude=latitude.toString()
       // splashViewModel.dataManager.longitude=longitude.toString()
        goToNextActivity()
    }

    @SuppressLint("MissingPermission")
    fun gettingLocationAfterPermissionGranted() {
        mFusedLocationClient!!.lastLocation
            .addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {

                    // Logic to handle location object
                    try {
                        val locationJson = JSONObject()
                        locationJson.put("latitude", location.latitude)
                        locationJson.put("longitude", location.longitude)
                        Log.d("latitude1", ":" + location.latitude)
                        Log.d("longitude1", ":" + location.longitude)
                        saveLatLongToLocal(location.latitude, location.longitude)

                        //moving to login activity
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
//                        Toast.makeText(this,"go to settings set location permission",Toast.LENGTH_LONG).show()
                    createLocationRequest()
                    val builder = LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest!!)
                    val client = LocationServices.getSettingsClient(this@SplashActivity)
                    val task = client.checkLocationSettings(builder.build())
                    task.addOnSuccessListener { // All location settings are satisfied. The client can initialize
                        // location requests here.
                        // ...
                        startLocationUpdates()
                    }
                    task.addOnFailureListener { e ->
                        if (e is ResolvableApiException) {
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                e.startResolutionForResult(this@SplashActivity as Activity,
                                    CHECK_LOCATION_SETTINGS_REQUEST_CODE)
                            } catch (sendEx: IntentSender.SendIntentException) {
                            }
                        }
                    }
                }
            }
    }

    private fun startLocationUpdates() {
//
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
            mLocationCallback,
            null /* Looper */)
    }

    protected fun createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = LocationRequest()
            mLocationRequest!!.interval = 10000
            mLocationRequest!!.fastestInterval = 5000
            mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CHECK_LOCATION_SETTINGS_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                startLocationUpdates()
            } else {
                Toast.makeText(this, "Please turn on location settings", Toast.LENGTH_SHORT).show()
                //                finish();
            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d("permissions","---"+ Gson().toJson(grantResults)+"sudip"+requestCode)

        permissionUtils!!.onRequestPermissionsResult(requestCode , permissions , grantResults)

    }




}