package com.example.myapplication

import android.os.Bundle
import android.util.Log
import android.widget.HorizontalScrollView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.api.UserListResponse
import com.example.myapplication.fragments.home.HomeFragment
import com.example.myapplication.fragments.home.HomeFragment.Companion.newInstance
import com.example.myapplication.fragments.map.MapFragment
import com.example.myapplication.fragments.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    var apiInterface: APIInterface? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        apiInterface = ApiClient.getClient()!!.create(APIInterface::class.java)



       // callApiUserListing()

        var bottomNavigationView= findViewById<BottomNavigationView>(R.id.nav_view)

transactionHome()
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {

                R.id.navigation_home -> {

                    transactionHome()

                    /*  if (fragmentManager!!.backStackEntryCount >= 1) {
                          fragmentManager!!.popBackStack(ArtistSearchFragment::class.java.getSimpleName(), FragmentManager.POP_BACK_STACK_INCLUSIVE)
                      }

                      getSupportFragmentManager()
                              .beginTransaction()
                              //  .disallowAddToBackStack()
  // .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                              .replace(R.id.content, ArtistSearchFragment.newInstance(), ArtistSearchFragment.TAG)
                              .addToBackStack(null)
                              .commit()
  */


                }
                R.id.navigation_map -> {

                    transactMap()



                }
                R.id.navigation_profile -> {

                    transactAccount()



                }



            }

            true
        }


    }

    private fun transactMap() {
//        TODO("Not yet implemented")


        supportFragmentManager
            .beginTransaction()
              .disallowAddToBackStack()
            // .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
            .replace(R.id.content, MapFragment.newInstance(), MapFragment.TAG)
          //  .addToBackStack(null)
            .commit()
    }

    private fun transactionHome() {
        supportFragmentManager
            .beginTransaction()
             .disallowAddToBackStack()
            // .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
            .replace(R.id.content, HomeFragment.newInstance(), HomeFragment.TAG)
//            .addToBackStack(null)
            .commit()    }

    private fun transactAccount() {
//        TODO("Not yet implemented")
        supportFragmentManager
            .beginTransaction()
             .disallowAddToBackStack()
            // .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
            .replace(R.id.content, ProfileFragment.newInstance(), ProfileFragment.TAG)
          //  .addToBackStack(null)
            .commit()

    }



    private fun callApiUserListing() {
        val call3: Call<UserListResponse?>? = apiInterface!!.createUser()
        call3!!.enqueue(object : Callback<UserListResponse?> {
            override fun onResponse(
                    call: Call<UserListResponse?>?,
                    response: Response<UserListResponse?>
            ) {
                var user =response.body()

                Log.d("response","--"+user!!.page)


            }

            override fun onFailure(call: Call<UserListResponse?>, t: Throwable?) {
                call.cancel()
            }
        })    }
}