package com.example.myapplication.api

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserListResponse {

    @SerializedName("page")
    @Expose
    public var page = 0

    @SerializedName("per_page")
    @Expose
    public var perPage = 0

    @SerializedName("total")
    @Expose
    public var total = 0

    @SerializedName("total_pages")
    @Expose
    public var totalPages = 0

    @SerializedName("data")
    @Expose
    public var data: List<UserResponseData>? = null

//    @SerializedName("support")
//    @Expose
//    public var support: Support? = null
}