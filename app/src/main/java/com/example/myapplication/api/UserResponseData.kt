package com.example.myapplication.api

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class UserResponseData {

    @SerializedName("id")
    @Expose
    public var id = 0

    @SerializedName("email")
    @Expose
    public var email: String? = null

    @SerializedName("first_name")
    @Expose
    public var firstName: String? = null

    @SerializedName("last_name")
    @Expose
    public var lastName: String? = null

    @SerializedName("avatar")
    @Expose
    public var avatar: String? = null

}
