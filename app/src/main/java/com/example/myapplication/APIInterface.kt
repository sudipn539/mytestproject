package com.example.myapplication

//import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.User
import com.example.myapplication.api.UserListResponse
import retrofit2.Call
import retrofit2.http.*


interface APIInterface {
//    @GET("/api/unknown")
//    fun doGetListResources(): Call<MultipleResource?>?



    @POST("/api/login")
    fun login(@Body user: User?): Call<User?>?


//    @FormUrlEncoded
    @GET("/api/users?page=2")
    fun createUser(): Call<UserListResponse?>?

//    @GET("/api/users?")
//    fun doGetUserList(@Query("page") page: String?): Call<UserList?>?

    @FormUrlEncoded
    @POST("/facefirst_ios_and_android/web/app-businesslatlong?")
    fun doCreateUserWithField(
        @Field("business_id") name: String?
//        @Field("job") job: String?
    ): Call<User?>?
}