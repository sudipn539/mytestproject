package com.example.myapplication.fragments.profile

import android.Manifest
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.myapplication.LoginActivity
import com.example.myapplication.R
import com.example.myapplication.User
import com.example.myapplication.fragments.home.HomeFragment
import com.example.myapplication.utils.AppConstants
import com.example.myapplication.utils.CameraHelper
import com.facefirst.utils.PermissionResultCallback
import com.facefirst.utils.PermissionUtils
import com.google.gson.Gson
import java.io.*
import java.util.ArrayList

class ProfileFragment : Fragment(),PermissionResultCallback {

    companion object {
        val TAG = ProfileFragment::class.java!!.simpleName

        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel

    internal lateinit var permissionUtils: PermissionUtils

    public var pathholder: String? =null
    public var path: String? =null
    lateinit var permissions : ArrayList<String>
//    lateinit var obj : RealPath
private val FILE_REQUEST_CODE_BY_CAMERA = 101
    private val FILE_REQUEST_CODE = 102
    var cameraCurrentImageTakenFile: File? = null
    var cameraCurrentImageTakenFileUri: Uri? = null
var profileImage:String?=null
    var imageview:ImageView?=null

    var uploadType:String?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }
    fun getMimeType(uri: Uri?): String? {
        val contentResolver: ContentResolver =activity!!.getContentResolver()
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(contentResolver.getType(uri!!))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        // TODO: Use the ViewModel

        permissionUtils = PermissionUtils(activity!!, this)
        permissions= ArrayList();
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        permissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permissions.add(android.Manifest.permission.CAMERA)





   //     permissionUtils.check_permission(permissions, getString(R.string.permission_required), AppConstants.REQUEST_CODE_STORAGE_PERMS)





        var submitBt=activity!!.findViewById<TextView>(R.id.loginBt)
        var nameEt=activity!!.findViewById<TextView>(R.id.editTextTextPersonName)
        var phoneEt=activity!!.findViewById<TextView>(R.id.phone_et)
        var passwordEt=activity!!.findViewById<TextView>(R.id.password_et)
        var emailEt=activity!!.findViewById<TextView>(R.id.email_et)


        val prefs: SharedPreferences =activity!!.getSharedPreferences(AppConstants.LOGIN, Context.MODE_PRIVATE)
        var   token = prefs.getString("token", "") //"No name defined" is the default value.
        var   emailP = prefs.getString("email", "") //"No name defined" is the default value.

        var   phoneP = prefs.getString("phone", "") //"No name defined" is the default value.
        var   passwordP = prefs.getString("password", "") //"No name defined" is the default value.
        var   nameP = prefs.getString("name", "") //"No name defined" is the default value.
        var   imageP = prefs.getString("image", "") //"No name defined" is the default value.






        if(!token.isNullOrEmpty()){

            nameEt.text = nameP
            phoneEt.text=phoneP
            passwordEt.text=passwordP
            emailEt.text=emailP

        }

        submitBt.setOnClickListener {
            var     name=nameEt.text.toString()
            var     password=passwordEt.text.toString()
            var     email=emailEt.text.toString()
            var     phone=phoneEt.text.toString()




            if( name.isNullOrEmpty()){
                Toast.makeText(activity!!,"please enter email ", Toast.LENGTH_LONG).show()
            }else if(phone.isNullOrEmpty()){
                Toast.makeText(activity!!,"please enter phone no ", Toast.LENGTH_LONG).show()

            }else if(phone.length!=10){
                Toast.makeText(activity!!,"please enter valid phone no ", Toast.LENGTH_LONG).show()

            }else if(email.isNullOrEmpty()){
                Toast.makeText(activity!!,"please enter an email ", Toast.LENGTH_LONG).show()

            }
            else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                Toast.makeText(activity!!,"please enter valid email ", Toast.LENGTH_LONG).show()


            else if(password.isNullOrEmpty()){
                Toast.makeText(activity!!,"password can't be blank", Toast.LENGTH_LONG).show()
            }else{

                val prefs: SharedPreferences.Editor=activity!!.getSharedPreferences(AppConstants.LOGIN, Context.MODE_PRIVATE).edit()
                     prefs.putString("phone", phone)
                      prefs.putString("email", email)
                      prefs.putString("password", password)
                     prefs.putString("name", name)


                prefs.apply()

                Toast.makeText(activity!!,"submit successfully",Toast.LENGTH_LONG).show()


//                Toast.makeText(this,"ok",Toast.LENGTH_LONG).show()


            }
        }







        var textView_logout=activity!!.findViewById<TextView>(R.id.logut_tv)

        textView_logout.setOnClickListener {

            val prefs: SharedPreferences =activity!!.getSharedPreferences(AppConstants.LOGIN, Context.MODE_PRIVATE)
//            prefs.edit().remove("token")

            prefs.edit().clear().apply()

            startActivity(Intent(activity!!,LoginActivity::class.java))
            activity!!.finish()
        }


        imageview=activity!!.findViewById<ImageView>(R.id.imageView)
        imageview!!.setOnClickListener {


            permissionUtils.check_permission(permissions, getString(R.string.permission_required), AppConstants.REQUEST_CODE_STORAGE_PERMS)

        }

    }



    override fun PermissionGranted(request_code: Int) {
//        Toast.makeText(activity,"permissions granted",Toast.LENGTH_LONG).show()


        Log.d("here","===")
        val dialogBuilder = AlertDialog.Builder(activity!!).setCancelable(true)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_file_upload, null)
        dialogBuilder.setView(dialogView)
        val camera = dialogView.findViewById(R.id.tv_camera_select) as TextView
        val library = dialogView.findViewById(R.id.tv_library_select) as TextView

        var   alertDialog = dialogBuilder.create()
        //  alertDialog.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
        alertDialog.show()

        camera.setOnClickListener {

//            val cam_intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
//
//            startActivityForResult(cam_intent, 123)
//
            uploadFileByCamera()
            alertDialog.dismiss()

        }
        library.setOnClickListener {

//            var intent = Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("*/*");
//            startActivityForResult(intent, 7);
            uploadFile()
            alertDialog.dismiss()
        }


    }

    private fun uploadFileByCamera() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraCurrentImageTakenFile = CameraHelper.saveAFileInExternalStorageAndGetTheFile(activity!!.applicationContext!!, ".jpeg")
        cameraCurrentImageTakenFileUri = CameraHelper.getTheUriOfTheFileToSendInTheCameraIntent(activity!!.applicationContext!!, cameraCurrentImageTakenFile)
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraCurrentImageTakenFileUri)
        startActivityForResult(takePictureIntent, FILE_REQUEST_CODE_BY_CAMERA)
        Log.v("cmraCrentImgTknFile", cameraCurrentImageTakenFile.toString())



    }

    fun uploadFile() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        val mimetypes = arrayOf("image/jpg", "image/jpeg", "image/png",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword",
                "application/pdf", "text/plain")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)
        startActivityForResult(intent, FILE_REQUEST_CODE)
    }

    override fun PartialPermissionGranted(
        request_code: Int,
        granted_permissions: ArrayList<String>
    ) {
        Toast.makeText(activity,"PartialPermissionGranted granted",Toast.LENGTH_LONG).show()

    }

    override fun PermissionDenied(request_code: Int) {
        Toast.makeText(activity,"PermissionDenied granted",Toast.LENGTH_LONG).show()

    }

    override fun NeverAskAgain(request_code: Int) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("requestCode", "requestCode--: $requestCode" +"------"+resultCode)

        if (resultCode == Activity.RESULT_OK) {

            Log.d("requestCode", "requestCode: $requestCode")

            if (requestCode == FILE_REQUEST_CODE_BY_CAMERA) {


//                profileimg=true

//                Log.v("profileimg", profileimg.toString())
//                val uri = data!!.data

                val mimeType: String = this!!.getMimeType(cameraCurrentImageTakenFileUri)!!
                val FILE_NAM = "appFile" + System.currentTimeMillis()
              //  outputfile = this.getFilesDir().toString() + File.separator + FILE_NAM + "_tmp." + mimeType
                val inputStream: InputStream = activity!!.getContentResolver().openInputStream(cameraCurrentImageTakenFileUri!!)!!
              //  Log.v("camraCrntImageTaFileUri", outputfile)
                Log.v("cmraCrentImgTknFile", cameraCurrentImageTakenFile.toString())
                Log.v("camraCrntImageTaFileUri", inputStream.toString())
                Glide.with(this).load(cameraCurrentImageTakenFileUri).apply(RequestOptions.circleCropTransform())

                        .placeholder(R.drawable.default_)
                        .error(R.drawable.default_).circleCrop().into(imageview!!)



//                serverCommonRequest.userprofile_image=cameraCurrentImageTakenFile




            } else if (requestCode == FILE_REQUEST_CODE) {

                Log.d("FILE_REQUEST_CODE", "FILE_REQUEST_CODE: ")

                val uri = data!!.data
                val mimeType: String = this!!.getMimeType(uri)!!
                val FILE_NAM = "appFile" + System.currentTimeMillis()
                val outputfile: String = activity!!.filesDir.toString() + File.separator + FILE_NAM + "_tmp." + mimeType
                val inputStream: InputStream = activity!!.contentResolver.openInputStream(uri!!)!!
                val fileToBeAdded: File? = this!!.createFileFromInputStream(inputStream, outputfile)
                cameraCurrentImageTakenFile=fileToBeAdded



//                profileimg=true

//                Log.v("profileimg-", profileimg.toString())
                Glide.with(this).load(uri).apply(RequestOptions.circleCropTransform())

                        .placeholder(R.drawable.default_)
                        .error(R.drawable.default_).circleCrop().into(imageview!!)

//                Glide.with(this).load(uri).into(mActivityWalletBinding.ivPrfl)
              //  getStreamByteFromImage(cameraCurrentImageTakenFile!!)




            }



        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d("permissions","---"+ Gson().toJson(grantResults)+"sudip"+requestCode)

        permissionUtils!!.onRequestPermissionsResult(requestCode , permissions , grantResults)

    }
    private fun createFileFromInputStream(inputStream: InputStream, fileName: String): File? {
        try {
            val f = File(fileName)
            f.setWritable(true, false)
            val outputStream: OutputStream = FileOutputStream(f)
            val buffer = ByteArray(1024)
            var length = 0
            while (inputStream.read(buffer).also { length = it } > 0) {
                outputStream.write(buffer, 0, length)
            }
            outputStream.close()
            inputStream.close()
            return f
        } catch (e: IOException) {
            println("error in creating a file")
            e.printStackTrace()
        }
        return null
    }


}