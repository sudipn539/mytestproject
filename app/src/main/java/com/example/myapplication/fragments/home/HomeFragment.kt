package com.example.myapplication.fragments.home

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.api.UserResponseData
import com.example.myapplication.fragments.home.adapter.CustomAdapter
import com.example.myapplication.utils.SimpleDividerItemDecoration
import com.example.myapplication.utils.Status
import com.google.gson.Gson
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    companion object {
        val TAG = HomeFragment::class.java!!.simpleName

        fun newInstance() = HomeFragment()
    }

    lateinit var customAdapter:CustomAdapter

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.blank_fragment, container, false)


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
//         TODO: Use the ViewModel

        viewModel.callApiUserListing()

        setRecyclerView()
        getLiveData()


    }

    private fun getLiveData() {
        viewModel.getUserListResponse().observe(this, androidx.lifecycle.Observer {

            when(it.status) {

                Status.SUCCESS -> {

                    // showToast(this,""+it.data!!.message)
                    Log.v("dataSearch----","data---"+ Gson().toJson(it.data))
                    Log.v("dataSearchinfo----","data---"+ Gson().toJson(it.data!!))
                    customAdapter.addAllItems(it.data.data)
                    Log.d("handleError" , "" + it.data!!.page)


                }

                Status.FAIL -> {

                }

                Status.LOADING -> {
                }
                Status.ERROR -> {

                }

            }

        })
    }

    private fun setRecyclerView() {
//        TODO("Not yet implemented")

        var userRecyclerView= activity!!.findViewById<RecyclerView>(R.id.rcv_user)


        var data : ArrayList<UserResponseData> = ArrayList()
//        data.add("test")
//        data.add("test")
//        data.add("ram")
        customAdapter= CustomAdapter(data,activity!!)
        Log.d("thisdata","--"+data)
        userRecyclerView.layoutManager= LinearLayoutManager(activity,
            LinearLayoutManager.VERTICAL,
            false)


        val simpleDividerItemDecoration= SimpleDividerItemDecoration(activity!!)
        userRecyclerView.addItemDecoration(simpleDividerItemDecoration)
        userRecyclerView.adapter=customAdapter
    }

}