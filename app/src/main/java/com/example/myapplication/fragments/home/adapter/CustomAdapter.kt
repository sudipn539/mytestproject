package com.example.myapplication.fragments.home.adapter

//import android.R

import android.content.Intent
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.UserDetails
import com.example.myapplication.api.UserResponseData


class CustomAdapter
/**
 * Initialize the dataset of the Adapter.
 *
 * @param dataSet String[] containing the data to populate views to be used
 * by RecyclerView.
 */(
    public var localDataSet: ArrayList<UserResponseData>,var
    activity: FragmentActivity
) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userName: TextView
        val userImage:ImageView
        val userEmail:TextView
        val layout:ConstraintLayout


        init {
            // Define click listener for the ViewHolder's View
            userName = view.findViewById(R.id.tv_username)
            userImage = view.findViewById(R.id.iv_userprofile)

            userEmail = view.findViewById(R.id.tv_useremail)
            layout=view.findViewById(R.id.ly)

        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_userlist, viewGroup, false)
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(
        viewHolder: ViewHolder,
        position: Int
    ) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.userName.text = localDataSet[position].firstName + " "+localDataSet[position].lastName

        viewHolder.userEmail.text = localDataSet[position].email


        Glide.with(activity)
            .load(localDataSet[position].avatar)
            .circleCrop()
            .into( viewHolder.userImage);



        viewHolder.layout.setOnClickListener {
            activity.startActivity(Intent(activity!!,UserDetails::class.java)
                .putExtra("image",localDataSet[position].avatar)
                .putExtra("firstname",localDataSet[position].firstName)
                .putExtra("lastname",localDataSet[position].lastName)
                .putExtra("email",localDataSet[position].email)


            )
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return localDataSet.size
    }

    fun addAllItems(data: List<UserResponseData>?) {

        localDataSet.addAll(data!!)

        notifyDataSetChanged()

    }

}