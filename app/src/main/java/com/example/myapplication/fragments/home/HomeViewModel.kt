package com.example.myapplication.fragments.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.APIInterface
import com.example.myapplication.ApiClient
import com.example.myapplication.api.UserListResponse
import com.example.myapplication.utils.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {
    // TODO: Implement the ViewModel

   var apiInterface = ApiClient.getClient()!!.create(APIInterface::class.java)
    private val userResponse = MutableLiveData<Resource<UserListResponse>>()

    public fun callApiUserListing() {
        val call3: Call<UserListResponse?>? = apiInterface!!.createUser()
        call3!!.enqueue(object : Callback<UserListResponse?> {
            override fun onResponse(
                call: Call<UserListResponse?>?,
                response: Response<UserListResponse?>
            ) {
                var user =response.body()

                Log.d("response","--"+user!!.page)
                    userResponse.postValue(Resource.success(user))



            }

            override fun onFailure(call: Call<UserListResponse?>, t: Throwable?) {
                call.cancel()
            }
        })
    }

    fun getUserListResponse(): LiveData<Resource<UserListResponse>> {
        return userResponse
    }




}