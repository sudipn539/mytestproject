package com.example.myapplication.fragments.map

import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.utils.AppConstants
import com.facefirst.utils.PermissionResultCallback
import com.facefirst.utils.PermissionUtils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*


class MapFragment : Fragment() , PermissionResultCallback {

    companion object {
        val TAG = MapFragment::class.java!!.simpleName

        fun newInstance() = MapFragment()
    }

    var myMarker: Marker? = null

    private lateinit var viewModel: MapViewModel
    private var mapFragment: SupportMapFragment? = null
    private var permissionUtils: PermissionUtils? = null
    private var permissions = ArrayList<String>()
    var mLocationRequest: LocationRequest? = null
    private var mLocationCallback: LocationCallback? = null
    final var mMap: GoogleMap? = null
var lattitude:String?=null
    var longitude:String?=null

    private var mFusedLocationClient: FusedLocationProviderClient? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.map_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        // TODO: Use the ViewModel

      //  mapFragment.getMapAsync(this)

        val prefs: SharedPreferences =activity!!.getSharedPreferences(AppConstants.LATTITUTDE_LONGITIDE, MODE_PRIVATE)
         lattitude =
            prefs.getString("latitude", "") //"No name defined" is the default value.

         longitude = prefs.getString("longitude", "") //0 is the default value.

        Log.d("lattitude","==="+lattitude)

        Log.d("longitude","==="+longitude)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?

        showMarkerOnMapForNearby(mapFragment!!)
//        val   mapFragment =activity!!.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment


    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun PermissionGranted(request_code: Int) {
    }

    override fun PartialPermissionGranted(request_code: Int, granted_permissions: ArrayList<String>) {
    }

    override fun PermissionDenied(request_code: Int) {

    }

    override fun NeverAskAgain(request_code: Int) {


    }

    fun showMarkerOnMapForNearby( mapFragment: SupportMapFragment) {

        // Log.d("lattitude")

        //   mMap.clear();
        //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(specialViewModel.getDataManager().getLatitude()),Double.parseDouble(specialViewModel.getDataManager().getLongitude())), 14f));
        //myMarker.remove();

                 //   .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_baseline_location_on_24))
                //    .draggable(false)


        // selectedMarkerForNearby = null;
        mapFragment.getMapAsync {
            mMap=it
//            it.ad
            mMap!!.clear()

            if(!lattitude.isNullOrEmpty())

            myMarker =
                mMap!!.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            lattitude!!.toDouble(),
                            longitude!!.toDouble()
                        )
                    ).title("current location")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.house))
                        .draggable(false)
                )

            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lattitude!!.toDouble(),longitude!!.toDouble()), 10f))

//                mMap!!.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap!!.getCameraPosition().target.latitude,mMap!!.getCameraPosition().target.longitude))
//                        .radius(700)
//                        .strokeColor(Color.RED)
//                        .fillColor(R.color.light_blue));


//                myMarker = mMap!!.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(0), longitudes.get(0))).title(mapAddressIds.get(0)).
//                        icon(BitmapDescriptorFactory.fromResource(R.drawable.locetorcurrent)).draggable(false));


//                final Circle[] circle = { mMap!!.addCircle(new CircleOptions()
//                        .center(new LatLng(mMap!!.getCameraPosition().target.latitude,mMap!!.getCameraPosition().target.longitude))
//                        .radius(900)
//                        .strokeColor(R.color.transparent)
//                        .fillColor(R.color.light_blue))};
            mMap!!.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragStart(marker: Marker) {}
                override fun onMarkerDrag(marker: Marker) {}
                override fun onMarkerDragEnd(marker: Marker) {
//                        latitude=String.valueOf(marker.getPosition().latitude);
//                        longitude= String.valueOf(marker.getPosition().longitude);
//
//                        Log.v("latitude", String.valueOf(marker.getPosition().latitude));
//                        Log.v("longitude", String.valueOf(marker.getPosition().longitude));
//                        getAddressFromLocation((marker.getPosition().latitude), marker.getPosition().longitude);
                }
            })

//                for (int i = 0; i < latitudes.size(); i++) {
//                    myMarker = mMap!!.addMarker(new MarkerOptions().position(new LatLng(latitudes.get(i), longitudes.get(i))).title(mapAddressIds.get(i)).
//                            icon(BitmapDescriptorFactory.fromResource(R.drawable.locator)).draggable(false));
//                }

//                if (latitudes.size() != 0) {
//                  //  mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitudes.get(0), longitudes.get(0)), 13f));
//
//                    try {
//                        mMap!!.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                            @Override
//                            public boolean onMarkerClick(Marker marker) {
//
//                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.loc));
//                                if (selectedMarkerForNearby != null) {
//                                    selectedMarkerForNearby.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.locator));
//                                }
//                                selectedMarkerForNearby = marker;
//                                String markerId = marker.getTitle();
//                               // onItemClickReturnString.onItemClick(markerId);
//                                return true;
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
            //mMap!!.setInfoWindowAdapter(new CustomInfoWindowAdapter(mContext, nearbyProductJsonArr));
        }
    }

}