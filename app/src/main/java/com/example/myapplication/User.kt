package com.example.myapplication

import com.google.gson.annotations.SerializedName




class User {


    @SerializedName("email")
    var email: String? = null


    @SerializedName("token")
    var token: String? = null
    @SerializedName("error")
    var error: String? = null



    @SerializedName("password")
    var password: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("job")
    var job: String? = null

    @SerializedName("id")
    var id: String? = null

    @SerializedName("status")
    var status: String? = null


    @SerializedName("createdAt")
    var createdAt: String? = null

    fun User(name: String?, job: String?) {
        this.name = name
        this.job = job
    }
}